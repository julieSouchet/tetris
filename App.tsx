import { StatusBar } from "expo-status-bar";
import React from "react";
import { Button, ScrollView, StyleSheet, View } from "react-native";
import GameBoard from "./src/components/GameBoard";
import { bgColor } from "./src/styles/colors";

// eslint-disable-next-line require-jsdoc
export default function App(): JSX.Element {
  const rotateRight = (): void => {
    // TODO
  };
  const rotateLeft = (): void => {
    // TODO
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <GameBoard />
      <View style={styles.controlContainer}>
        <Button title="droite" onPress={rotateRight} />
        <Button title="gauche" onPress={rotateLeft} />
      </View>
      <StatusBar style="auto" />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: bgColor,
    alignItems: "center",
    justifyContent: "flex-start",
  },
  controlContainer: {
    marginTop: 20,
  },
});
