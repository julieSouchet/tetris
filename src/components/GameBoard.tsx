import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { borderColor } from "../styles/colors";
import textStyle from "../styles/textStyle";

const GameBoard = (): JSX.Element => {
  return (
    <View style={styles.container}>
      <Text style={textStyle.text}>(Game board)</Text>
    </View>
  );
};

export default GameBoard;

const styles = StyleSheet.create({
  container: {
    borderColor: borderColor,
    borderWidth: 2,
    width: "90%",
    margin: 5,
  },
});
