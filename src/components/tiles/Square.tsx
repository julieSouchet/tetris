import React, { useRef } from "react";
import { StyleSheet, Animated, Easing } from "react-native";

const Square = (): JSX.Element => {
  const rotationAnim = useRef(new Animated.Value(0)).current;
  const topAnim = useRef(new Animated.Value(0)).current;

  const rotateRight = (): void => {
    rotate(90);
  };

  const rotateLeft = (): void => {
    rotate(-90);
  };

  const rotate = (rotationOffset: number): void => {
    const newValue =
      (Number.parseInt(JSON.stringify(rotationAnim), 10) + rotationOffset) %
      360;
    Animated.timing(rotationAnim, {
      toValue: newValue,
      duration: 250,
      useNativeDriver: true,
    }).start(() => {
      rotationAnim.setValue(newValue);
    });
  };

  const rotateStyle = rotationAnim.interpolate({
    inputRange: [0, 360],
    outputRange: ["0deg", "360deg"],
  });

  const fall = (): void => {
    Animated.loop(
      Animated.timing(topAnim, {
        toValue: 800, // TODO: get screen heigth
        duration: 5000,
        useNativeDriver: true,
        isInteraction: false,
        easing: Easing.in(Easing.ease),
      })
    ).start();
  };

  const topStyle = topAnim.interpolate({
    inputRange: [0, 1000],
    outputRange: ["0px", "1000px"],
  });

  fall();

  return (
    <Animated.View style={{ transform: [{ translateY: topStyle }] }}>
      <Animated.View
        style={[styles.square, { transform: [{ rotate: rotateStyle }] }]}
      />
    </Animated.View>
  );
};

export default Square;

const size = 40;
const squareBgColor = "#E22";
const squareBorderColor = "#C00";
const squareBorderAccentColor = "#22E";
const styles = StyleSheet.create({
  square: {
    height: size,
    width: size,
    backgroundColor: squareBgColor,
    borderColor: squareBorderColor,
    borderWidth: 2,
    borderTopColor: squareBorderAccentColor,
    margin: 5,
  },
});
