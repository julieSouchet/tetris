/**
 * Represents a 2D point.
 */
export class Point {
  x = 0;
  y = 0;

  // eslint-disable-next-line require-jsdoc
  constructor(x = 0, y = 0) {
    this.x = x;
    this.y = y;
  }

  /**
   * Creates a new Point from this, with x added to it
   * @param {number} x
   * @returns {Point}
   */
  plusX(x: number): Point {
    return new Point(this.x + x, this.y);
  }

  /**
   * Creates a new Point from this, with y added to it
   * @param {number} y
   * @returns {Point}
   */
  plusY(y: number): Point {
    return new Point(this.x, this.y + y);
  }
}
