import { Point } from "./Point";

/**
 * Represents a basic tile (ie. a single square)
 */
class ComponentTile {
  static size = 40;
  origin: Point;
  rotation: number;
  isTop: boolean;

  /**
   * Initializes a component tile (ie. a single square)
   * @param {Point} origin the upper-left point, default = (0,0)
   * @param {boolean} isTop whether this component tile is placed at the top of its composite tile, default = false
   * @param {number} rotation the tile's rotation, in degrees, default = 0°
   */
  constructor(origin: Point = new Point(), isTop = false, rotation = 0) {
    this.origin = origin;
    this.isTop = isTop;
    this.rotation = rotation;
  }

  /**
   * Rotates the tile to the right
   */
  turnRight(): void {
    this.rotation = (this.rotation + 90) % 360;
  }
  /**
   * Rotates the tile to the left
   */
  turnLeft(): void {
    this.rotation = (this.rotation - 90) % 360;
  }

  /**
   * Moves the tile to the right
   */
  moveRight(): void {
    this.origin.x = this.origin.x + ComponentTile.size;
  }
  /**
   * Moves the tile to the left
   */
  moveLeft(): void {
    this.origin.x = this.origin.x - ComponentTile.size;
  }
  /**
   * Moves the tile down
   */
  moveDown(): void {
    this.origin.y = this.origin.y + ComponentTile.size;
  }

  /**
   * Checks whether the two tiles are colliding or not
   * @param {ComponentTile} tile the other tile
   * @returns {boolean} true if they are colliding, false if not
   */
  isColliding(tile: ComponentTile): boolean {
    return (
      Math.abs(this.origin.x - tile.origin.x) <= ComponentTile.size ||
      Math.abs(this.origin.y - tile.origin.y) <= ComponentTile.size
    );
  }
}

/**
 * Represents a tile as used on the game board
 */
abstract class CompositeTile extends ComponentTile {
  tilelist: ComponentTile[] = [];
  /**
   * Checks whether two composite tiles are colliding by checking each component tile against each other
   * @param {CompositeTile} tile the other composite tile that is checked for collision with this one
   * @returns {boolean} true if collision, false if not
   */
  isColliding(tile: CompositeTile): boolean {
    // Tests for collision each component tile of this composite tile against each component tile of the other composite
    // There only needs to be one collision for the two composite tiles to be colliding
    return this.tilelist.reduce(
      (totalIsColliding: boolean, curTile: ComponentTile) => {
        return (
          totalIsColliding ||
          // is the current component colliding with any of the other tile's components ?
          tile.tilelist.reduce(
            (currentTileIsColliding: boolean, otherTile: ComponentTile) => {
              return currentTileIsColliding || otherTile.isColliding(curTile);
            },
            false
          )
        );
      },
      false
    );
  }

  /**
   * Rotates the tile left, recalculating its components' positions
   */
  turnLeft(): void {
    super.turnLeft();

    const updatedTilelist = this.tilelist.map((tile: ComponentTile) => {
      /*
      Rotation matrix, with t being the rotation's angle:
      R = [ cos(t) -sin(t)
            sin(t) cos(t)]
      
      Previous position P1 = [x
                              y]
      
      New position P2 = R * P1
                      = [ cos(t)*x + (-sin(t))*y
                          sin(t)*x + cos(t)*y ]

      Here, t = -90°, or 3*pi/2 rad
      and cos(3*pi/2) = 0, sin(3*pi/2) = -1.

      Thus, P2 = [ 0*x + 1*y
                  -1*x + 0*y ]
              = [y
                -x]
      */
      return new ComponentTile(
        new Point(tile.origin.y, -tile.origin.x),
        tile.isTop,
        this.rotation
      );
    });

    this.tilelist = updatedTilelist;
  }

  /**
   * Rotates the tile right, recalculating its components' positions
   */
  turnRight(): void {
    super.turnRight();

    const updatedTilelist = this.tilelist.map((tile: ComponentTile) => {
      /*
      As above with turnLeft().

      Here, t = 90°, or pi/2 rad
      and cos(pi/2) = 0, sin(pi/2) = 1.

      Thus, P2 = [ 0*x + (-1)*y
                  1*x + 0*y ]
              = [-y
                x]
      */
      return new ComponentTile(
        new Point(-tile.origin.y, tile.origin.x),
        tile.isTop,
        this.rotation
      );
    });

    this.tilelist = updatedTilelist;
  }
}

/**
 * Represents a square tile
 *
 * [-] [-]
 *
 * [-] [-]
 */
export class Square extends CompositeTile {
  // eslint-disable-next-line require-jsdoc
  constructor(origin: Point) {
    super();
    this.tilelist = [
      new ComponentTile(origin, true), // upper left
      new ComponentTile(origin.plusX(ComponentTile.size), true), // upper right
      new ComponentTile(origin.plusY(ComponentTile.size)), // bottom left
      new ComponentTile(
        origin.plusY(ComponentTile.size).plusX(ComponentTile.size)
      ), // bottom right
    ];
  }
}
