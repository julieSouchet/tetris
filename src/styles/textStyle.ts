import { StyleSheet } from "react-native";
import { textColor } from "./colors";

export default StyleSheet.create({
  text: {
    color: textColor,
    fontFamily: "sans-serif",
  },
});
