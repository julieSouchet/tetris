import { Animated } from "react-native";

export const getNumberFromAnimatedValue = (value: Animated.Value): number =>
  Number.parseInt(JSON.stringify(value));
